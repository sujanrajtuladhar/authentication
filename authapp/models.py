from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Blog(models.Model):
	title =	models.CharField(max_length=200)
	image = models.ImageField(upload_to="blogs")
	content = models.TextField()
	date = models.DateTimeField(auto_now_add=True)
	author = models.ForeignKey(User, on_delete=models.CASCADE)

	def __str__(self):
		return self.title


class Comment(models.Model):
	blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
	commenter = models.ForeignKey(User, on_delete=models.CASCADE)
	comment = models.TextField()
	date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.commenter


SEMESTER = (
	("I", "First Semester"),
	("II", "II"),
	("III", "III"),
	("IV", "IV"),
)

class Course(models.Model):
	title = models.CharField(max_length=100)
	semester = models.CharField(max_length=50, choices=SEMESTER)

	def __str__(self):
		return self.title